#include <stdio.h>
#include <stdlib.h>

#include "fptypes.h"

int main(int argc, char const *argv[])
{
	float a = 1.5;
	float b = -3.75;
	float result_add, result_mul;
	float_t v1 = fp_encode(a);
  	float_t v2 = fp_encode(b);
  	float_t m_mul = fp_mul(v1, v2);
  	float_t m_add = fp_add(v1, v2);
  	result_add = fp_decode(m_add);
  	result_mul = fp_decode(m_mul);
  	printf("add result = %f mul result = %f\n", result_add, result_mul);
	return 0;
}