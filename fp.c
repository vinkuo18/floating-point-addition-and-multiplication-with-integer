#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "fptypes.h"



static float_t example_val = {2, 3, 4};

static inline float_t round_even(int mode, float_t input);


/*	 Returns Inf value */
static inline float_t get_Inf(void) {
	float_t answer = {0x0, 0xFF, 0x0};
	// TODO: add your code to represent Inf
	return answer;
}

/*	 Returns NaN value */
float_t get_NaN(void) {
	float_t answer = {0x0, 0xFF, 0x1};
	// TODO: add your code to represent NaN
	return answer;
}

/*	 Returns float value for 1 */
float_t get_1(void) {
	float_t answer = {0x0, 0x7F, 0x0};
	// TODO: add your code to represent 1
	return answer;
}

/*	 Returns float value for 0 */
float_t get_0(void) {
	float_t answer = {0x0, 0x0, 0x0};
	// TODO: add your code to represent 0
	return answer;
}

/*	 Returns Smallest denormalized value */
float_t get_smallest_denormalized(void) {
	float_t answer = {0x0, 0x0, 0x1};
	// TODO: add your code to represent smallest denormalized value
	return answer;
}



/* Given a C float data type, returns the equivalent float_t data
 * struct */
float_t fp_encode(float b) {
	// TIP: You may want to use a union with a uint32_t, shift and bitwise
	// operators for this.	Also, think about whether the endianness of your
	// machine will affect the way you want to interpret the float.
	uint32_t int_bits;
	union float_converter fc;
	fc.floatval = b;
	int_bits = fc.intval;


	float_t answer = example_val;
	uint32_t mask;
	mask = (1 << (FRAC_BITS)) - 1;
	//printf("%x\n", mask);
	answer.mantissa = int_bits & mask;
	int_bits >>= FRAC_BITS;
	mask = (1 << (EXPONENT_BITS)) - 1;
	answer.exponent = int_bits & mask;
	int_bits >>= EXPONENT_BITS;
	answer.sign = int_bits & 0x1;
	return answer;
}

/* Given a float_t struct, returns the equivalent float C data type */
float fp_decode(float_t a) {
	// TIP: Just revert what you have done in above function. Easy!!!
	uint32_t int_bits = 0;
	uint32_t mask;
	union float_converter fc;
	int_bits |= (a.sign & 0x1);
	int_bits <<= EXPONENT_BITS;
	mask = (1 << (EXPONENT_BITS)) - 1;
	int_bits |= (a.exponent & mask);

	int_bits <<= FRAC_BITS;
	mask = (1 << (FRAC_BITS)) - 1;
	int_bits |= (a.mantissa & mask);
	fc.intval = int_bits;


	float answer = fc.floatval;
	return answer;
}

/* Compute -a, return the result */
float_t fp_negate(float_t a) {
	float_t answer = a;
	answer.sign = !(answer.sign);
	return answer;
}

/* Add two float_t numbers, return the result */
float_t fp_add(float_t a, float_t b) {
	// mode 0: pos+pos denorm
	// mode 1: pos+pos one denorm one norm
	// mode 2: pos+pos two norm
	// mode 3: pos+neg denorm
	// mode 4: pos+neg one denorm one norm
	// mode 5: pos+neg two norm
	int exp_diff = 0;
	int mode = 0; //mode for rounding
	float_t answer = example_val;
	//putting back the leading one or zero
	if(a.exponent == 0 && b.exponent == 0){
		mode = 0;
	}
	else if(a.exponent == 0 && b.exponent != 0){
		mode = 1;
		b.mantissa |= (1<<FRAC_BITS);
		a.exponent += 1; // for exp_diff calculation
	}
	else if(a.exponent != 0 && b.exponent == 0){
		mode = 1;
		a.mantissa |= (1<<FRAC_BITS);
		b.exponent += 1; // for exp_diff calculation
	}
	else{
		mode = 2;
		a.mantissa |= (1<<FRAC_BITS);
		b.mantissa |= (1<<FRAC_BITS);
	}
	//same sign:
	if(!(a.sign ^ b.sign)){
		answer.sign = a.sign;

		if(a.exponent > b.exponent){
			exp_diff = a.exponent - b.exponent;
			if(exp_diff <= 23){
					a.mantissa <<= exp_diff;
					answer.exponent = b.exponent;
			}
			else{
				answer.mantissa = a.mantissa;
				answer.exponent = a.exponent;
			}
		}
		else if(a.exponent < b.exponent){
			exp_diff = b.exponent - a.exponent;
			if(exp_diff <= 23){
				b.mantissa <<= exp_diff;
				answer.exponent = a.exponent;
			}
			else{
				answer.mantissa = b.mantissa;
				answer.exponent = b.exponent;
			}
		}
		else{
			answer.exponent = a.exponent;
		}
		if(exp_diff <= 23){
			answer.mantissa = a.mantissa + b.mantissa;
			answer = round_even(mode, answer);
		}
	}
	else{
		//printf("signed addition!\n");
		mode += 3;
		if(a.exponent > b.exponent){
			answer.sign = a.sign;
			exp_diff = a.exponent - b.exponent;
			if(exp_diff <= 23){
					a.mantissa <<= exp_diff;
					answer.exponent = b.exponent;
			}
			else{
				answer.mantissa = a.mantissa;
				answer.exponent = a.exponent;
			}
		}
		else if(a.exponent < b.exponent){
			answer.sign = b.sign;
			exp_diff = b.exponent - a.exponent;
			if(exp_diff <= 23){
				b.mantissa <<= exp_diff;
				answer.exponent = a.exponent;
				b.mantissa ^= a.mantissa;
				a.mantissa ^= b.mantissa;
				b.mantissa ^= a.mantissa;
			}
			else{
				answer.mantissa = b.mantissa;
				answer.exponent = b.exponent;
			}
		}
		else{
			if(a.mantissa > b.mantissa){
				answer.sign = a.sign;
			}
			else{
				answer.sign = b.sign;
				b.mantissa ^= a.mantissa;
				a.mantissa ^= b.mantissa;
				b.mantissa ^= a.mantissa;
			}
			answer.exponent = a.exponent;
		}
		if(exp_diff <= 23){
			answer.mantissa = a.mantissa - b.mantissa;
			answer = round_even(mode, answer);
		}
	}

	
	
	return answer;
}

/* Multiply two float_t numbers, return the result */
float_t fp_mul(float_t a, float_t b) {

	// modes: 6 : denorm * denorm, 7: norm * denorm, 8: norm * norm
	float_t answer = example_val;
	uint8_t sign;
	int mode = 6; //denorm * denorm
	answer.sign = (a.sign ^ b.sign)&0x1;
	answer.exponent = a.exponent + b.exponent - BIAS(EXPONENT_BITS);
	if(answer.exponent < 0){
		return get_0();
	}
	if (answer.exponent >= ((1<<EXPONENT_BITS)-1)){
		sign = answer.sign;
		answer = get_Inf();
		answer.sign = sign;
		return answer;
	}
	
	if (a.exponent==0 && b.exponent == 0){
		mode = 6;
	}
	else if(a.exponent==0){
		b.mantissa |= (1<<FRAC_BITS);
		mode = 7;
	}
	else if(b.exponent == 0){
		a.mantissa |= (1<<FRAC_BITS);
		mode = 7;
	}
	else{
		a.mantissa |= (1<<FRAC_BITS);
		b.mantissa |= (1<<FRAC_BITS);
		mode = 8;
	}
	answer.mantissa = a.mantissa * b.mantissa;
	answer = round_even(mode, answer);
	return answer;
}

static inline float_t round_even(int mode, float_t input){
	//printf("mode = %d\n", mode);
	float_t answer = input;
	uint8_t sign;
	uint64_t tmp = input.mantissa;
	int to_shift = -FRAC_BITS;
	uint64_t guard = 0;
	uint64_t round = 0;
	uint64_t stick = 0;
	if (mode == 8 || mode == 7){
		to_shift = -(2*FRAC_BITS);
	}
	if(tmp != 0){
		while(tmp != 1){
		tmp >>= 1;
		to_shift++;
		}
	}
	
	if(to_shift > 0){
		guard = (input.mantissa >> to_shift) & 0x1;
		round = (input.mantissa >> (to_shift - 1)) & 0x1;
		if(to_shift > 1){
			stick = input.mantissa & ((1<<(to_shift - 1)) -1);
		}
	}
	if(mode == 0){
		if((input.mantissa >> FRAC_BITS) > 0){
			answer.exponent += 1;
			answer.mantissa &= ((1<<FRAC_BITS)-1);
		}
	}
	else if(mode == 1 || mode == 2){
		if(to_shift > 0){
			answer.mantissa >>= to_shift;
			answer.exponent += to_shift;
		}
		
		if(round != 0 && stick != 0){
			answer.mantissa += 1;
		}
		else if(guard != 0 && round != 0 && stick == 0){
			answer.mantissa += 1;
		}
		//check if mantissa overflows
		if(answer.mantissa & (1<<(FRAC_BITS+1))){
			answer.mantissa >>= 1;
			answer.exponent += 1;
		}
		answer.mantissa &= ((1<<FRAC_BITS)-1);
		if((answer.exponent) >= ((1<<EXPONENT_BITS)-1) ){ //overflow
			sign = answer.sign;
			answer = get_Inf();
			answer.sign = sign;
		}
	}
	else if(mode == 4 || mode==5){
		if(to_shift > 0){
			answer.mantissa >>= to_shift;
			answer.exponent += to_shift;
		}
		else if(to_shift < 0){
			if (answer.exponent > (-to_shift))
				answer.exponent += (to_shift);
			else if (answer.exponent == (-to_shift)){
				answer.exponent = 1;
				to_shift = 0;
			}
			else{
				to_shift = -(answer.exponent);
				answer.exponent = 0;
			}
			guard = 0;
			round = 0;
			stick = 0;
			answer.mantissa <<= -to_shift;
		}

		if(round != 0 && stick != 0){
			answer.mantissa += 1;
		}
		else if(guard != 0 && round != 0 && stick == 0){
			answer.mantissa += 1;
		}

	}

	else if(mode == 6){
		guard = (input.mantissa >> FRAC_BITS) & 0x1;
		round = (input.mantissa >> (FRAC_BITS - 1)) & 0x1;
		stick = input.mantissa & ((1<<(FRAC_BITS - 1)) -1);
		answer.mantissa >>= (FRAC_BITS);
		if(round != 0 && stick != 0){
			answer.mantissa += 1;
		}
		else if(guard != 0 && round != 0 && stick == 0){
			answer.mantissa += 1;
		}

	}

	else if(mode == 7){
		if (to_shift == 0){
			answer.exponent += 1;
		}

		if (to_shift < 0){
			if (answer.exponent > (-to_shift))
				answer.exponent += (to_shift +1);
			else if (answer.exponent == (-to_shift)){
				answer.exponent = 1;
				to_shift = 0;
			}
			else{
				to_shift = -(answer.exponent);
				answer.exponent = 0;
			}
		}

		guard = (input.mantissa >> (FRAC_BITS + to_shift)) & 0x1;
		round = (input.mantissa >> (FRAC_BITS + to_shift - 1)) & 0x1;
		stick = input.mantissa & ((1<<(FRAC_BITS + to_shift - 1)) -1);
		answer.mantissa >>= (FRAC_BITS + to_shift);
		
		if(round != 0 && stick != 0){
			answer.mantissa += 1;
		}
		else if(guard != 0 && round != 0 && stick == 0){
			answer.mantissa += 1;
		}
		answer.mantissa &= ((1<<FRAC_BITS)-1);
		if((answer.exponent) >= ((1<<EXPONENT_BITS)-1) ){ //overflow
			sign = answer.sign;
			answer = get_Inf();
			answer.sign = sign;
		}
	}

	else if(mode == 8){
		
		if(to_shift < 0){
			to_shift = 0;
		}
		guard = (input.mantissa >> (FRAC_BITS + to_shift)) & 0x1;
		round = (input.mantissa >> (FRAC_BITS + to_shift - 1)) & 0x1;
		stick = input.mantissa & ((1<<(FRAC_BITS + to_shift - 1)) -1);
		answer.mantissa >>= (FRAC_BITS + to_shift);
		answer.exponent += to_shift;
		if(round != 0 && stick != 0){
			answer.mantissa += 1;
		}
		else if(guard != 0 && round != 0 && stick == 0){
			answer.mantissa += 1;
		}
		answer.mantissa &= ((1<<FRAC_BITS)-1);
		if((answer.exponent) >= ((1<<EXPONENT_BITS)-1) ){ //overflow
			sign = answer.sign;
			answer = get_Inf();
			answer.sign = sign;
		}
	}
	return answer;
}
