To use the functions to perform floating point addition and multiplication with integer:

1. Encode the float into self-defined float_t with the function "fp_encode(float b)"
2. Perform addition with function "fp_add(float_t a, float_t b)",
   and multiplicatio with function "fp_mul(float_t a, float_t b)"
3. After operations, use function "fp_decode(float_t a)" to decode float_t into float

See example.c for usage examples.